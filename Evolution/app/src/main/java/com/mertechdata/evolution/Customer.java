package com.mertechdata.evolution;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.THttpClient;
import java.util.concurrent.ExecutionException;

/**
 * Created by David on 6/17/2015.
 */
public class Customer extends Activity {
    Button b1;
    Button b2;
    tCustomer customer;
    String [] separated = null;
    Boolean message = false;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super .onCreate(savedInstanceState);

        setContentView(R.layout.customer);
        TextView t4 = (TextView)findViewById(R.id.textView4);
        TextView t5 = (TextView)findViewById(R.id.textView5);
        TextView t6 = (TextView)findViewById(R.id.textView6);
        TextView t7 = (TextView)findViewById(R.id.textView7);
        TextView t10 = (TextView)findViewById(R.id.textView10);
        TextView t12 = (TextView)findViewById(R.id.textView12);
        TextView t13 = (TextView)findViewById(R.id.textView13);
        b1 = (Button)findViewById(R.id.updateButton);
        b2 = (Button)findViewById(R.id.deleteButton);

        try {
            customer = new MyTask().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        t4.setText(customer.getName());
        t5.setText(customer.getAddress());
        t6.setText(customer.getState());
        t7.setText(customer.getPhone_Number());
        t10.setText(customer.getEmail_Address());
        t12.setText(customer.getZip());
        t13.setText(customer.getCity());
        b1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                final String session_id = extras.getString("session_id");
                TextView name = (EditText) findViewById(R.id.textView4);
                TextView address = (EditText) findViewById(R.id.textView5);
                TextView city = (EditText) findViewById(R.id.textView13);
                TextView phoneNumber = (EditText) findViewById(R.id.textView7);
                TextView state = (EditText) findViewById(R.id.textView6);
                TextView zip = (EditText) findViewById(R.id.textView12);
                TextView email = (EditText) findViewById(R.id.textView10);
                String phoneExpression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
                String emailExpression = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";;

                if (name.getText().toString().equals("") || address.getText().toString().equals("") || city.getText().toString().equals("") || state.getText().toString().equals("") || zip.getText().toString().equals("")) {
                    alertUser("FIELDS ARE BLANK");
                } else if (!phoneNumber.getText().toString().matches(phoneExpression)) {
                    alertUser("INVALID PHONE NUMBER");
                } else if (!email.getText().toString().matches(emailExpression)) {
                    alertUser("INVALID EMAIL");
                } else {
                    try {
                        customer = new UpdateCustomer().execute().get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    final TextView popup = new TextView(Customer.this);
                    popup.setText("Updated");
                    popup.setGravity(Gravity.CENTER);
                    popup.setTextSize(16);
                    popup.setPadding(0, 40, 0, 40);
                    final AlertDialog.Builder popupView = new AlertDialog.Builder(Customer.this);
                    popupView.setView(popup);
                    final AlertDialog alert = popupView.create();
                    alert.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            alert.dismiss();
                        }
                    }, 2 * 1000);
                    Intent intent = new Intent(Customer.this, CustomerList.class);
                    intent.putExtra("session_id", session_id);
                    startActivity(intent);
                }
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                final String session_id = extras.getString("session_id");
                AlertDialog.Builder builder1 = new AlertDialog.Builder(Customer.this);
                builder1.setMessage("Are you sure?");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    new DeleteCustomer().execute().get();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                }
                                dialog.cancel();
                                if (message != true) {
                                    alertUser("CUSTOMER DID NOT DELETE");
                                } else {
                                    final TextView popup = new TextView(Customer.this);
                                    popup.setText("Deleted");
                                    popup.setGravity(Gravity.CENTER);
                                    popup.setTextSize(16);
                                    popup.setPadding(0, 40, 0, 40);
                                    final AlertDialog.Builder popupView = new AlertDialog.Builder(Customer.this);
                                    popupView.setView(popup);
                                    final AlertDialog alert = popupView.create();
                                    alert.show();
                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            alert.dismiss();
                                        }
                                    }, 2 * 1000);
                                    Intent intent = new Intent(Customer.this, CustomerList.class);
                                    intent.putExtra("session_id", session_id);
                                    startActivity(intent);
                                }
                            }
                        });
                builder1.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    private void alertUser(String message) {
        final TextView popup = new TextView(Customer.this);
        popup.setText(message);
        popup.setGravity(Gravity.CENTER);
        popup.setTextSize(16);
        popup.setPadding(0, 40, 0, 40);
        popup.setTextColor(Color.WHITE);
        popup.setBackgroundColor(Color.RED);
        final AlertDialog.Builder popupView = new AlertDialog.Builder(Customer.this);
        popupView.setView(popup);
        final AlertDialog alert = popupView.create();
        alert.show();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                alert.dismiss();
            }
        }, 2 * 1000);
    }

    private class MyTask extends AsyncTask<Void, Void, tCustomer > {
        Bundle extras = getIntent().getExtras();
        String session_id = extras.getString("session_id");
        int custNum = extras.getInt("custNum");
        String custName = extras.getString("custName");
        @Override
        protected tCustomer doInBackground(Void... params) {
            String url = "https://evolution.mertechdata.com/OEDemo";
            THttpClient transport;
            try {
                transport = new THttpClient(url);
            } catch (Exception e) {
                return null;
            }
            TBinaryProtocol theProtocol = new TBinaryProtocol(transport);
            EvoOrder.Client server = new EvoOrder.Client(theProtocol);
            try {
                customer = server.FindCustomerByName(session_id, custName, custNum, 2);
                return customer;
            } catch (TException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(tCustomer result) {
            super.onPostExecute(result);
        }
    }

    private class UpdateCustomer extends AsyncTask<Void, Void, tCustomer> {

        @Override
        protected tCustomer doInBackground(Void... params) {
            Bundle extras = getIntent().getExtras();
            int custNum = extras.getInt("custNum");
            String session_id = extras.getString("session_id");

            TextView name = (EditText)findViewById(R.id.textView4);
            TextView address = (EditText)findViewById(R.id.textView5);
            TextView city = (EditText) findViewById(R.id.textView13);
            TextView phoneNumber = (EditText)findViewById(R.id.textView7);
            TextView email = (EditText)findViewById(R.id.textView10);
            TextView state = (EditText) findViewById(R.id.textView6);
            TextView zip = (EditText) findViewById(R.id.textView12);
            customer.setCustomer_Number(custNum);
            customer.setName(name.getText().toString());
            customer.setAddress(address.getText().toString());
            customer.setCity(city.getText().toString());
            customer.setState(state.getText().toString());
            customer.setZip(zip.getText().toString());
            customer.setPhone_Number(phoneNumber.getText().toString());
            customer.setEmail_Address(email.getText().toString());
            String url = "https://evolution.mertechdata.com/OEDemo";
            THttpClient transport;
            try {
                transport = new THttpClient(url);
            } catch (Exception e) {
                return null;
            }
            TBinaryProtocol theProtocol = new TBinaryProtocol(transport);
            EvoOrder.Client server = new EvoOrder.Client(theProtocol);
            try {
                customer = server.AddEditCustomer(session_id, customer);
                return customer;
            } catch (TException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private class DeleteCustomer extends AsyncTask<Void, Void, tCustomer> {

        @Override
        protected tCustomer doInBackground(Void... params) {
            Bundle extras = getIntent().getExtras();
            int custNum = extras.getInt("custNum");
            String session_id = extras.getString("session_id");

            String url = "https://evolution.mertechdata.com/OEDemo";
            THttpClient transport;
            try {
                transport = new THttpClient(url);
            } catch (Exception e) {
                return null;
            }
            TBinaryProtocol theProtocol = new TBinaryProtocol(transport);
            EvoOrder.Client server = new EvoOrder.Client(theProtocol);
            try {
                message = server.DeleteCustomer(session_id, custNum);
            } catch (TException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
