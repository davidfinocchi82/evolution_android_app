package com.mertechdata.evolution;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.THttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by David on 6/16/2015.
 */
public class CustomerList extends Activity {
    List<tCustomerList> customers;
    private ListView lv;
    private int offset = 0;
    private int page_size = 30;
    List<String> customerName = new ArrayList<>();
    List<Integer> customerNumber = new ArrayList<>();
    Button b1;
    Button b2;
    String search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super .onCreate(savedInstanceState);
        setContentView(R.layout.customer_list);
        b1 = (Button)findViewById(R.id.addCustomerButton);
        b2 = (Button)findViewById(R.id.searchButton);
        try {
            customers = new MyTask().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        lv = (ListView) findViewById(R.id.listView);
        lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastVisibleItem;
            private int totalItemCount;
            private boolean isEndOfList;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.totalItemCount = totalItemCount;
                this.lastVisibleItem = firstVisibleItem + visibleItemCount - 1;
                if (totalItemCount > visibleItemCount)
                    checkEndOfList();
            }

            private synchronized void checkEndOfList() {
                // trigger after 2nd to last item
                if (lastVisibleItem >= (totalItemCount - 10)) {
                    if (!isEndOfList) {
                        page_size += 30;
                        try {
                            customers = new MyTask().execute().get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        for(int i = 0; i < customers.size(); i++){
                            customerName.add(customers.get(i).Name + "\n" + customers.get(i).Phone_Number);
                            customerNumber.add(customers.get(i).Customer_Number);
                        }
                        ArrayAdapter<String> items = new ArrayAdapter<String>(CustomerList.this, android.R.layout.simple_list_item_1, customerName);
                        lv.setAdapter(items);
                        lv.setSelection(totalItemCount -20);
                    }
                    isEndOfList = true;
                } else {
                    isEndOfList = false;
                }
            }
        });
        for(int i = 0; i < customers.size(); i++){
            customerName.add(customers.get(i).Name + "\n" + customers.get(i).Phone_Number);
            customerNumber.add(customers.get(i).Customer_Number);
        }
        ArrayAdapter<String> items = new ArrayAdapter<String>(CustomerList.this, android.R.layout.simple_list_item_1, customerName);
        lv.setAdapter(items);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle extras = getIntent().getExtras();
                String session_id = extras.getString("session_id");
                int custNum = customerNumber.get(position);
                String[] custName = customerName.get(position).split("\n");
                Intent intent = new Intent(CustomerList.this, Customer.class);
                intent.putExtra("session_id", session_id);
                intent.putExtra("custNum", custNum);
                intent.putExtra("custName", custName[0]);
                startActivity(intent);
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                String session_id = extras.getString("session_id");

                Intent intent = new Intent(CustomerList.this, AddCustomer.class);
                intent.putExtra("session_id", session_id);
                startActivity(intent);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(CustomerList.this);

                alert.setTitle("Search");

                final EditText input = new EditText(CustomerList.this);
                input.setHint("Search By Name");
                alert.setView(input);

                alert.setPositiveButton("Go!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle extras = getIntent().getExtras();
                        String session_id = extras.getString("session_id");
                        search = input.getText().toString();

                        Intent intent = new Intent(CustomerList.this, CustomerList.class);
                        intent.putExtra("session_id", session_id);
                        intent.putExtra("search_filter", search);
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alert.show();
            }
        });
    }

    private class MyTask extends AsyncTask<Void, Void, List<tCustomerList> > {
        @Override
        protected List<tCustomerList> doInBackground(Void... params) {

            Bundle extras = getIntent().getExtras();
            String session_id = extras.getString("session_id");
            String filter = extras.getString("search_filter");
            String url = "https://evolution.mertechdata.com/OEDemo";
            List<tCustomerList> customers = null;

            THttpClient transport;
            try {
                transport = new THttpClient(url);
            } catch (Exception e) {
                return null;
            }
            TBinaryProtocol theProtocol = new TBinaryProtocol(transport);
            EvoOrder.Client server = new EvoOrder.Client(theProtocol);
            try {
                customers = server.GetPageOfCustomers(session_id, offset, page_size, "Name", "ASC", filter);
            } catch (TException e) {
                e.printStackTrace();
            }
            return customers;
        }
        @Override
        protected void onPostExecute(List<tCustomerList> result) {
            super.onPostExecute(result);
        }
    }
}
