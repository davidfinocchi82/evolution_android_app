package com.mertechdata.evolution;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.THttpClient;

import java.util.concurrent.ExecutionException;

/**
 * Created by David on 6/22/2015.
 */
public class AddCustomer extends Activity {
    tCustomer customer;
    Button b1;
    String [] separated = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super .onCreate(savedInstanceState);
        setContentView(R.layout.add_customer);
        Bundle extras = getIntent().getExtras();
        final String session_id = extras.getString("session_id");
        b1 = (Button) findViewById(R.id.addCustToApi);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView name = (EditText) findViewById(R.id.textView12);
                TextView address = (EditText) findViewById(R.id.textView13);
                TextView phoneNumber = (EditText) findViewById(R.id.textView15);
                TextView email = (EditText) findViewById(R.id.textView16);
                TextView state = (EditText) findViewById(R.id.textView6);
                TextView zip = (EditText) findViewById(R.id.textView2);
                TextView city = (EditText) findViewById(R.id.textView7);
                String phoneExpression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
                String emailExpression = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

                if (name.getText().toString().equals("") || address.getText().toString().equals("") || city.getText().toString().equals("") || state.getText().toString().equals("") || zip.getText().toString().equals("")) {
                    alertUser("FIELDS ARE BLANK");
                } else if (!phoneNumber.getText().toString().matches(phoneExpression)) {
                    alertUser("INVALID PHONE NUMBER");
                } else if (!email.getText().toString().matches(emailExpression)) {
                    alertUser("INVALID EMAIL");
                } else {
                    try {
                        new UpdateCustomer().execute().get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    final TextView popup = new TextView(AddCustomer.this);
                    popup.setText("Added");
                    popup.setGravity(Gravity.CENTER);
                    popup.setTextSize(16);
                    popup.setPadding(0, 40, 0, 40);
                    final AlertDialog.Builder popupView = new AlertDialog.Builder(AddCustomer.this);
                    popupView.setView(popup);
                    final AlertDialog alert = popupView.create();
                    alert.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            alert.dismiss();
                        }
                    }, 2 * 1000);
                    Intent intent = new Intent(AddCustomer.this, CustomerList.class);
                    intent.putExtra("session_id", session_id);
                    startActivity(intent);
                }
            }
        });
    }

    private void alertUser(String message) {
        final TextView popup = new TextView(AddCustomer.this);
        popup.setText(message);
        popup.setGravity(Gravity.CENTER);
        popup.setTextSize(16);
        popup.setPadding(0, 40, 0, 40);
        popup.setTextColor(Color.WHITE);
        popup.setBackgroundColor(Color.RED);
        final AlertDialog.Builder popupView = new AlertDialog.Builder(AddCustomer.this);
        popupView.setView(popup);
        final AlertDialog alert = popupView.create();
        alert.show();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                alert.dismiss();
            }
        }, 2 * 1000);
    }

    private class UpdateCustomer extends AsyncTask<Void, Void, tCustomer> {
        Bundle extras = getIntent().getExtras();
        String session_id = extras.getString("session_id");

        TextView name = (EditText) findViewById(R.id.textView12);
        TextView address = (EditText) findViewById(R.id.textView13);
        TextView city = (EditText) findViewById(R.id.textView7);
        TextView state = (EditText) findViewById(R.id.textView6);
        TextView zip = (EditText) findViewById(R.id.textView2);
        TextView phoneNumber = (EditText) findViewById(R.id.textView15);
        TextView email = (EditText) findViewById(R.id.textView16);
        String[] seperated = city.getText().toString().split(" ");

        @Override
        protected tCustomer doInBackground(Void... params) {
                customer = new tCustomer();
                customer.setName(name.getText().toString());
                customer.setAddress(address.getText().toString());
                customer.setCity(city.getText().toString());
                customer.setState(state.getText().toString());
                customer.setZip(zip.getText().toString());
                customer.setPhone_Number(phoneNumber.getText().toString());
                customer.setEmail_Address(email.getText().toString());
                customer.setCredit_Limit(0.0);
                customer.setPurchases(0.0);
                customer.setBalance(0.0);
                customer.setStatus("Y");
                String url = "https://evolution.mertechdata.com/OEDemo";
                THttpClient transport;
                try {
                    transport = new THttpClient(url);
                } catch (Exception e) {
                    return null;
                }
                TBinaryProtocol theProtocol = new TBinaryProtocol(transport);
                EvoOrder.Client server = new EvoOrder.Client(theProtocol);
                try {
                    customer = server.AddEditCustomer(session_id, customer);
                    return customer;
                } catch (TException e) {
                    e.printStackTrace();
                }
                return null;
        }
    }
}
