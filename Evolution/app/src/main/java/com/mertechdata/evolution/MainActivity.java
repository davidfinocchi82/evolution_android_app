package com.mertechdata.evolution;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.THttpClient;

import java.util.concurrent.ExecutionException;

public class MainActivity extends Activity {
    Button b1;
    EditText ed1, ed2;

    String session_id;
    String search_filter = "";

    int counter = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.button);
        ed1 = (EditText)findViewById(R.id.editText1);
        ed2 = (EditText)findViewById(R.id.editText2);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    session_id = new MyTask().execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    session_id = e.toString();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    session_id = e.toString();
                }
                if (session_id.length() == 36) {
                    Intent intent = new Intent(MainActivity.this, CustomerList.class);
                    intent.putExtra("session_id", session_id);
                    intent.putExtra("search_filter", search_filter);
                    startActivity(intent);
                } else {
                    final TextView popup = new TextView(MainActivity.this);
                    popup.setText("INVALID LOGIN");
                    popup.setGravity(Gravity.CENTER);
                    popup.setTextSize(16);
                    popup.setPadding(0, 40, 0, 40);
                    popup.setTextColor(Color.WHITE);
                    popup.setBackgroundColor(Color.RED);
                    final AlertDialog.Builder popupView = new AlertDialog.Builder(MainActivity.this);
                    popupView.setView(popup);
                    final AlertDialog alert = popupView.create();
                    alert.show();
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            alert.dismiss();
                        }
                    }, 2 * 1000);
                }
            }
        });
    }
    private class MyTask extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            String username, password, url, session;
            url = "https://evolution.mertechdata.com/OEDemo";
            username = ed1.getText().toString();
            password = ed2.getText().toString();
            THttpClient transport;
            try {
                transport = new THttpClient(url);
            } catch (Exception e) {
                return null;
            }
            TBinaryProtocol theProtocol = new TBinaryProtocol(transport);
            EvoOrder.Client server = new EvoOrder.Client(theProtocol);
            tUser user = new tUser();
            user.setUsername(username);
            user.setPassword(password);
            try {
                session = server.UserLogin(user);
            } catch (Exception e){
                e.printStackTrace();
                session = e.toString();
            }
            return session;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
